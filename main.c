#include <std.h>

#include <structs/ll.h>

#include <exit.h>
#include <execvp.h>
#include <fork.h>
#include <getchar.h>
#include <malloc.h>
#include <memset.h>
#include <puts.h>
#include <strcmp.h>
#include <strcpy.h>
#include <strlen.h>
#include <strtok.h>
#include <waitpid.h>


#define INPUT_LIMIT 1024
#define MAX_TOKENS 64


struct ll *alias_root = NULL, *builtins_root = NULL;
struct ll_alias_node {
	char *cmd, *tokens[MAX_TOKENS];
	unsigned char len;
};


void populate_tokens(char **tokens, char *input, unsigned char *tokens_length, unsigned char resolve_aliases){
	if(!strlen(input))
		return;

	tokens[*tokens_length] = strtok(input, " ");
	if(tokens[*tokens_length] == NULL)
		return;

	if(resolve_aliases){
		struct ll *node = alias_root;
		static unsigned char count;

		while(node != NULL && strcmp(node->index, tokens[*tokens_length]))
			node = node->next;

		if(node != NULL){
			for(count = 0; count < ((struct ll_alias_node*)node->value)->len; count++,(*tokens_length)++)
				tokens[*tokens_length] = ((struct ll_alias_node*)node->value)->tokens[count];
		}else
			(*tokens_length)++;
	}else
		(*tokens_length)++;

	tokens[*tokens_length] = strtok(NULL, " ");
	while(tokens[*tokens_length] != NULL){
		(*tokens_length)++;
		if(*tokens_length >= MAX_TOKENS)
			puts("Too many arguments...?");

		tokens[*tokens_length] = strtok(NULL, " ");
	}
}

void add_alias(char *alias, char *cmds){
	struct ll_alias_node *alias_node = malloc(sizeof(struct ll_alias_node));
	struct ll *node = malloc(sizeof(struct ll));

	alias_node->cmd = malloc(strlen(cmds)+1);
	strcpy(alias_node->cmd, cmds);

	memset(alias_node->tokens, 0, sizeof(char*)*MAX_TOKENS);
	alias_node->len = 0;

	populate_tokens((char**)&(alias_node->tokens), alias_node->cmd, &(alias_node->len), 0);

	node->index = alias;
	node->value = alias_node;
	node->next = alias_root;
	alias_root = node;
}

void add_builtin(char *cmd, void* func){
	struct ll *node = malloc(sizeof(struct ll));
	node->index = cmd;
	node->value = func;
	node->next = builtins_root;
	builtins_root = node;
}

unsigned char run_builtin(char **tokens){
	struct ll *node = builtins_root;
	while(node != NULL && strcmp(node->index, tokens[0]))
		node = node->next;

	if(node == NULL)
		return 0;

	((void(*)(char**))node->value)(tokens);
	return 1;
}

void run_tokens(char **tokens){
	pid_t cpid, pid = fork();

	if(pid < 0){
		puts("Error forking process");
		exit(1);
	}else if(!pid){
		if(execvp(tokens[0], tokens) == -1){
			puts("execvp failed");
			exit(1);
		}
	}else{
		static int status;
		do{
			cpid = waitpid(pid, &status, WUNTRACED);
		}while(!(cpid == pid && (WIFEXITED(status) || WIFSIGNALED(status))));
	}
}

void _start(){
	#include "init-builtins.c"

	add_alias("ls", "ls --color=auto");
	add_alias("ll", "ls -al --color=auto");
	add_alias("grep", "grep --colour=auto");
	add_alias("egrep", "egrep --colour=auto");
	add_alias("fgrep", "fgrep --colour=auto");

	char input_buffer[INPUT_LIMIT+1] = {0}, *tokens[MAX_TOKENS] = {NULL};
	unsigned char tokens_length = 0;
	unsigned short input_len = 0;
	int input_char;

	print_prompt:
	puts("(test-shell) > ");

	tokens_length = 0;
	memset(input_buffer, 0, sizeof(char)*INPUT_LIMIT);
	input_len = 0;

	for(;;){
		input_char = getchar();
		switch(input_char){
			case EOF:
				exit(0);
			break;

			case '\n':
			case '\r':
			case '\a':
				populate_tokens((char**)&tokens, input_buffer, &tokens_length, 1);
				if(!run_builtin(tokens))
					run_tokens(tokens);

				goto print_prompt;

			case '\b':
				input_buffer[input_len--] = 0;
			break;

			default:
				input_buffer[input_len++] = input_char;
			break;
		}
	}

	puts("Your loop broke lol");

	exit(0);
}
