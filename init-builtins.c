#ifdef BUILTIN_CD
	void shell_cd(char**);

	add_builtin("cd", (void*)shell_cd);
#endif

#ifdef BUILTIN_EXIT
	void shell_exit(char**);

	add_builtin("quit", (void*)shell_exit);
	add_builtin("exit", (void*)shell_exit);
#endif
