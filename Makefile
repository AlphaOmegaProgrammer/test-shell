CC=gcc -nostdlib -L/path/to/test-c-lib -I/path/to/test-c-lib
CFLAGS=-g -Wall -Wextra -pedantic -std=c11
DEFINES=-DBUILTIN_EXIT -DBUILTIN_CD

all: cd exit main final

cd:
	$(CC) -c builtins/cd.c $(CFLAGS) -l-chdir -o objs/cd.o

exit:
	$(CC) -c builtins/exit.c $(CFLAGS) -l-exit -o objs/exit.o

main:
	$(CC) -c $(DEFINES) main.c $(CFLAGS) -o objs/main.o

final:
	$(CC) $(CFLAGS) objs/*.o -l-chdir -l-execvp -l-exit -l-fork -l-getchar -l-malloc -l-puts -l-memset -l-strcmp -l-strcpy -l-strlen -l-strtok -l-waitpid -o test-shell



clean:
	rm test-shell objs/*
